# sisop-praktikum-fp-2023-mh-it12

### Laporan Resmi Final Project Praktikum Sistem Operasi 

**Anggota Kelompok IT12:**

| Nama                      | NRP        |
|---------------------------|------------|
|Muhammad Afif              | 5027221032 |
|Hafiz Akmaldi Santosa      | 5027221061 |
|Nur Azka Rahadiansyah      | 5027221064 |

# Pengantar

Laporan resmi ini dibuat terkait dengan praktikum Final Project sistem operasi yang telah dilaksanakan pada tanggal xx Desember 2023 hingga tanggal xx Desember 2023. Final Project terdiri dari 1 soal dengan tema C yang dikerjakan oleh kelompok praktikan yang terdiri dari 3 orang selama waktu tertentu.
Kelompok IT12 melakukan pengerjaan Final Project ini dengan pembagian sebagai berikut:


- Autentikasi, Autorisasi, Logging,Reliability, Tambahan dan Error Handling dikerjakan oleh M Afif.
- Data Definition Languange dikerjakan oleh Nur Azka.
- Data Manipulation Languange Hafiz A.

Sehingga dengan demikian, semua anggota kelompok memiliki peran yang sama besar dalam pengerjaan Final Project ini.

Kelompok IT12 juga telah menyelesaikan tugas Final Project yang telah diberikan dan telah melakukan demonstrasi kepada Asisten lab penguji Mas Dzakir. Dari hasil Final Project yang telah dilakukan sebelumnya, maka diperoleh hasil sebagaimana yang dituliskan pada setiap bab di bawah ini.


# Soal & Penyelesaian Final Praktikum

### Akses Program
Program server berjalan sebagai daemon, memungkinkan akses console database melalui program client. Interaksi antara client dan server utamanya terjadi melalui socket, yang memungkinkan program client untuk mengakses server dari mana saja dengan koneksi yang tepat.

### Struktur Direktori
Struktur direktori ini mengandung tiga folder utama: `database`, `client`, dan `dump`. Program-program inti disimpan dalam direktori ini, memfasilitasi manajemen sistem database dan komunikasi antara server dan client.

### Penggunaan Database
Autentikasi pengguna memanfaatkan username dan password yang terhubung dengan akses database yang sesuai. Hak akses terhadap database disimpan di dalam database itu sendiri, namun hanya root yang memiliki akses untuk melihatnya. Proses pembuatan user baru hanya dapat dilakukan oleh user root, sedangkan pengguna hanya bisa mengakses database yang telah diberi izin oleh root.

### Data Definition Language (DDL)
Perihal struktur database, tabel, dan kolom dapat dibuat menggunakan perintah khusus. Semua pengguna memiliki kapabilitas untuk membuat database, tapi mereka harus memperoleh izin untuk mengaksesnya. Kemampuan untuk membuat tabel di sebuah database hanya tersedia bagi root dan pengguna yang memiliki izin untuk database tersebut. Penghapusan (DROP) database, tabel, dan kolom memiliki format yang ditetapkan.

### Data Manipulation Language (DML)
Berbagai operasi manipulasi data seperti INSERT, UPDATE, DELETE, SELECT disediakan oleh sistem. Batasan pada jumlah operasi per perintah diterapkan, dan kondisi WHERE hanya menggunakan tanda '='.

### Logging
Semua perintah yang dijalankan harus tercatat dalam log dengan format yang telah ditentukan. Hal ini memungkinkan untuk melacak aktivitas yang dilakukan dalam sistem.

### Reliability
Ada program terpisah yang bertanggung jawab untuk melakukan dump database, dengan proses autentikasi yang terintegrasi. Proses ini dilakukan secara berkala sesuai jadwal waktu tertentu dan hasilnya di-zip dengan penamaan berdasarkan timestamp.

### Tambahan
Kemampuan untuk memasukkan perintah melalui file dengan menggunakan redirection di program client merupakan fitur tambahan yang disediakan.

### Error Handling
Apabila ada perintah yang tidak sesuai, sistem akan memberikan pesan error tanpa keluar dari program client. Hal ini memastikan kelancaran penggunaan sistem meskipun terjadi kesalahan pada perintah yang dimasukkan.


Berikut merupakan hasil kerja kelompok  `IT-12`
## Client
Berikut merupakan isi dari file client.c :

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#define PORT 8080
#define BUFFER_SIZE 1024

void to_use() {
    printf("To use this database program:\n");
    printf("> Run as Root: sudo ./client\n");
    printf("> Login as user: ./client -u <username> -p <password>\n");
    printf("Username \"Admin\" is not allowed\n");
}

int mksock() {
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) return -1;

    // Menetapkan informasi alamat server
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Konversi alamat IP dari string ke format biner
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) return -2;

    // Melakukan koneksi ke server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) return -3;

    return sock;
}

//menerima dan mengirim program database
void prog_run() {
    while(1) {
        char cmdreq[BUFFER_SIZE];
        char buffer[BUFFER_SIZE] = {0};
        int sock = 0;
        int status_cek=0;

        //Input Message
        printf("> ");
        fgets(cmdreq, BUFFER_SIZE, stdin);
        cmdreq[strcspn(cmdreq, "\n")] = '\0';

        // Membuat socket
        sock = mksock();
        if(sock < 0) {
            printf("An error has occured while creating socket, please try again. Error(%d)\n", sock);
            continue;
        }

        //Mengirim command ke server
        status_cek = send(sock, cmdreq, strlen(cmdreq), 0);
        if(status_cek == -1) {
            perror("Failed to send command\n");
            close(sock);
            continue;
        }
        status_cek = 0;

        //Output dari server database
        status_cek = read(sock, buffer, BUFFER_SIZE);
        if(status_cek == -1) {
            perror("Failed to read output\n");
            close(sock);
            break;
        }
        printf("%s, %d\n", buffer, (int)strlen(buffer));
        if(strcmp(buffer, "Turning off database") == 0) break;

        close(sock);
    }
}

int main(int argc, char *argv[]) {
    char *loginreq = malloc(BUFFER_SIZE);
    char buffer[BUFFER_SIZE];

    //Autentikasi
    int sudo_kah = (geteuid() == 0);

    if(sudo_kah) { //Autentikasi root
        if(argc != 1) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN root root");

    } else { //Autentikasi user biasa
        char *username = NULL;
        char *passw = NULL;

        if(strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            to_use();
            return 1;
        }
        username = argv[2];
        passw = argv[4];

        if(username == NULL || passw == NULL || (strcmp(username, "root") == 0)) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN %s %s", username, passw);
        
    }

    int sock = 0;
    struct sockaddr_in serv_addr;

    // Membuat socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    // Menetapkan informasi alamat server
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Konversi alamat IP dari string ke format biner
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    // Melakukan koneksi ke server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed\n");
        exit(EXIT_FAILURE);
    }

    //mengirim izin login
    send(sock, loginreq, strlen(loginreq), 0);
    free(loginreq);

    // Menerima respon dari server
    recv(sock, buffer, BUFFER_SIZE, 0);
    close(sock);

    if(strcmp(buffer, "logged in") == 0) {

        //jika berhasil, jalankan program database
        printf("Welcome to database\n");
        prog_run();

    } else {
        printf("%s\n", buffer);
        return 1;
    }
    return 0;
}

```

**Penjelasan :**

sebagai klien yang dapat terhubung ke server yang sudah disediakan pada alamat IP `127.0.0.1` dengan port `8080`. Fungsionalitas utamanya adalah untuk berinteraksi dengan program server database.c melalui pesan-pesan yang dikirim dan diterima melalui koneksi *socket*.

```main() ``` : Fungsi utama yang memulai eksekusi program. Menangani autentikasi pengguna dan berinteraksi dengan server untuk menjalankan program database.

```to_use()```: Digunakan untuk menampilkan pesan tentang cara menggunakan program database kepada pengguna.

```mksock()``` : Membuat socket dan melakukan koneksi ke server dengan alamat IP 127.0.0.1 dan port 8080. ```connectToServer()``` untuk melakukan koneksi ke server.

```prog_run()``` : Berisi loop yang memungkinkan pengguna untuk memasukkan perintah dan mengirimnya ke server. Selanjutnya, menerima output dari server dan menampilkannya. ```sendCommand()``` untuk  mengirim perintah ke server dan ```receiveOutput()``` untuk menerima output dari server.

## Database
Berikut merupakan isi dari file Database.c :
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <ctype.h>

#define PORT 8080
#define MAX_PENDING_CONNECTIONS 10
#define BUFF_SIZE 1024
#define SMOL_BUF 256
#define MAX_SIZE 100
#define MAX_STR_LENGTH 100
#define MAX_LENGTH 100
#define MAX_ROWS 100
#define MAX_ARR_SIZE 100
#define MAX_USERS 10
#define MAX_COLUMNS 10

const char *db_path = "databases/";
const char *db_users = "databases/user_db/users.txt";

char logged_user[32];
char db_used[32];

struct Column {
    char *column;
    char *datatype;
};

//Menghitung jumlah custom argumen DB
int arg_count(const char *argm, int target) {
    int count = 0;
    for(int i=0; i<strlen(argm); i++) {
        if(argm[i] == ' ') count++;
    }
    count++;
    if(count != target) return 0; //nol berarti fek
    return 1; //satu berarti rill
}

//Tampilkan error tanpa harus keluar dari program
void invalid_cmd(int new_socket) {
    char *server_message = "Invalid Command!";
    printf("Send message: %s\n", server_message);
    send(new_socket, server_message, strlen(server_message), 0);
}

//Kirim pesan instan ke client
void snd_msg(int new_socket, const char *msg_to_send) {
    printf("Send message: %s\n", msg_to_send);
    send(new_socket, msg_to_send, strlen(msg_to_send), 0);
}

//Cek database ada apa nda
int isDbExist(const char *db_name) {
    char db_ril_path[SMOL_BUF];
    sprintf(db_ril_path, "%s/%s", db_path, db_name);
    struct stat stats;

    stat(db_ril_path, &stats);
    if (S_ISDIR(stats.st_mode)) return 1;
    return 0;
}

//Cek table ada apa nda
int isTableExist(const char *db_name, const char *table_name) {
    char table_ril_path[SMOL_BUF];
    sprintf(table_ril_path, "%s/%s/%s.txt", db_path, db_name, table_name);

    if(access(table_ril_path, F_OK)) return 1;
    return 0;
}

//Periksa apakah user ada di daftar user dan apakah passwordnya sesuai
int user_check(const char *uname, const char *passw) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        usr_token = strtok(NULL, ";");
        char *temp_passw = usr_token;

        if(strcmp(uname, temp_uname) == 0 && strcmp(passw, temp_passw) == 0) {
            fclose(fp);
            return 0;
        }
    }

    fclose(fp);
    return 1;
}

//Periksa apakah user terdaftar atau tidak
int user_exist(const char *uname) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        if(strcmp(uname, temp_uname) == 0) {
            fclose(fp);
            return 0;
        }
    }

    fclose(fp);
    return 1;
}

//Fungsi login user
void db_login(int new_socket, char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    char *uname = comen;

    comen = strtok(NULL, " ");
    char *pass = comen;

    if(strcmp(uname, "root") == 0 && strcmp(pass, "root") == 0) {
        sprintf(res_msg, "logged in");
        printf("User logged in\n");
        strcpy(logged_user, uname);

    } else {
        int checkin = user_check(uname, pass);
        if(checkin == 0) {
            sprintf(res_msg, "logged in");
            printf("User logged in\n");
            strcpy(logged_user, uname);
        }
        else if(checkin > 0) sprintf(res_msg, "invalid username / password");
        else sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    }
    send(new_socket, res_msg, (strlen(res_msg)+1), 0);
}

//Fungsi tulis user baru
int writ_usr(const char *new_uname, const char *new_passw) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        if(strcmp(new_uname, temp_uname) == 0) {
            fclose(fp);
            return 1;
        }
    }
    fclose(fp);

    FILE *nfp = fopen(db_users, "a");
    if(!nfp) return -1;
    fprintf(nfp, "%s;%s\n", new_uname, new_passw);
    fclose(nfp);
    return 0;
}

//Fungsi bikin user
void mkusr(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to create new user, access denied\n", logged_user);
        char *server_message = "You dont have any permissions to do that!";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    char *uname_new = comen;
    if(strcmp(uname_new, "root") == 0) {
        char *server_message = "User name is not allowed";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "IDENTIFIED") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "BY") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *passw_new = comen;

    int checkin = writ_usr(uname_new, passw_new);

    if(checkin < 0) sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    else if(checkin > 0) sprintf(res_msg, "Username %s is already taken", uname_new);
    else if(checkin == 0) sprintf(res_msg, "Added username %s", uname_new);

    send(new_socket, res_msg, strlen(res_msg), 0);
}

//Fungsi cari permissions user
int find_db_perms(const char *db_name, const char *u_name) {
    char access_buf[SMOL_BUF] = {0};
    char newline[SMOL_BUF];
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_name, db_name);

    if(access(access_buf, F_OK)) return -2; //cek apakah file perms ada, jika ada maka db juga ada

    FILE *fp = fopen(access_buf, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';
        if(strcmp(newline, u_name) == 0) {
            fclose(fp);
            return 1;
        }
    }
    fclose(fp);
    return 0;
}

//Fungsi Grant Permissions db
void grnt_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "PERMISSION") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *db_check = comen;

    if(strcmp(db_check, "user_db") == 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "INTO") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *to_user = comen;

    int stawtus = user_exist(to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s is not registered", to_user);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the user data file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    stawtus = 0;

    stawtus = find_db_perms(db_check, to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s already have permissions to %s database", to_user, db_check);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_check, db_check);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "%s\n", to_user);
    fclose(fp);

    sprintf(res_msg, "User %s has been granted permission to %s database", to_user, db_check);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi untuk menggunakan database
void set_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    comen = strtok(NULL, " ");
    char *db_target = comen;

    int stawtus = find_db_perms(db_target, logged_user);
    if(stawtus == 0) {
        sprintf(res_msg, "User %s dont have permission to use %s database", logged_user, db_target);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    strcpy(db_used, db_target);
    sprintf(res_msg, "You are now using %s database", db_target);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi membuat database
void CreateDatabase(int new_socket, const char *comen) {
    char res_msg[SMOL_BUF] = {0};
    comen = strtok(NULL, " ");

    char *db_name = comen;
    if(isDbExist(db_name)) {
        sprintf(res_msg, "Database %s is already exist!", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    char command[BUFF_SIZE + strlen(db_name) + 100];
    snprintf(command, sizeof(command), "mkdir -p %s/%s",db_path ,db_name);
    int status = system(command);

    if (status != 0) {
        strcpy(res_msg, "Gagal create database");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_name, db_name);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "permissions string\n");
    if(strcmp(logged_user, "root") == 0) fprintf(fp, "root\n");
    else fprintf(fp, "root\n%s\n", logged_user);
    fclose(fp);

    strcpy(res_msg, "Berhasil create database");
    snd_msg(new_socket, res_msg);
    printf("%s\n", res_msg);
}

//Fungsi membuat tabel
void CreateTable(int new_socket, const char *comen) {
    char path[SMOL_BUF];
    char res_msg[BUFF_SIZE];
    snprintf(path, sizeof(path), "%s/%s",db_path, db_used);

    comen = strtok(NULL, " ");
    char *table_name = comen;
    if(isTableExist(db_used, table_name) == 1) {
        sprintf(res_msg, "Table %s is already exist!", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE];
    char columnData[BUFF_SIZE * MAX_COLUMNS * 20];
    char dest[BUFF_SIZE];

    snprintf(command, sizeof(command), "touch %s/%s.txt", path, table_name);
    system(command);

    comen = strtok(NULL, " ");
    char *query = comen;

    char *start = strchr(query, '(') + 1;
    char *end = strchr(query, ')');
    if (end != NULL) *end = '\0';

    struct Column cols[MAX_COLUMNS];
    int numCols = 0;

    char *token = strtok(start, " ");
    while (token != NULL) {
        snprintf(cols[numCols].column, sizeof(cols[numCols].column), "%s", token);
        token = strtok(NULL, ",");
        if (token != NULL) {
            snprintf(cols[numCols].datatype, sizeof(cols[numCols].datatype), "%s", token);
            token = strtok(NULL, " ");
        }
        numCols++;
    }

    command[0] = '\0';
    snprintf(command, sizeof(command), "echo ");

    for (int i = 0; i < numCols; ++i) {
        snprintf(columnData, sizeof(columnData), "%s %s;", cols[i].column, cols[i].datatype);
        strcat(command, columnData);
        if (i != numCols - 1) {
            strcat(command, " ");
        }
    }

    snprintf(dest, sizeof(dest), " > %s/%s.txt", path, table_name);
    strcat(command, dest);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Table %s created in Database %s", table_name, db_used);
    printf("Table %s created in Database %s\n", table_name, db_used);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus database
void DropDatabase(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *db_name = comen;
    char *end = strchr(db_name, ';');   
    if (end != NULL) *end = '\0';

    if(isDbExist(db_name) == 0) {
        sprintf(res_msg, "%s is not exist", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    if(find_db_perms(db_name, logged_user) == 0) {
        sprintf(res_msg, "%s have no permission to %s database", logged_user, db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    
    char command[MAX_STR_LENGTH * 2];
    snprintf(command, sizeof(command), "rm -rf %s/%s", db_path, db_name);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Database %s berhasil dihapus", db_name);
    printf("%s\n", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus table berdasarkan database yang sedang digunakan
void DropTable(int new_socket, const char *comen) {
    char path[BUFF_SIZE];
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *table_name = comen;
    char *end = strchr(table_name, ';');   
    if (end != NULL) *end = '\0';

    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_used, table_name);

    if(isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s is not exist in current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE + 100];
    snprintf(command, sizeof(command), "rm -r %s", path);
    system(command);

    snprintf(res_msg, BUFF_SIZE, "Table %s berhasil dihapus", table_name);
    printf("%s", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

void DropColumn(const char *db_name, const char *table_name, const char *column_name, int is_sudo, const char *user) {
    char path[MAX_STR_LENGTH];
    char message[BUFF_SIZE];
    snprintf(path, sizeof(path), "/home/tzubast/FP\\ SISOP/database/databases/%s/%s.txt", db_name, table_name);

    if (!is_sudo && strcmp(user, "admin") != 0) {
        snprintf(message, MAX_STR_LENGTH, "User %s tidak memiliki akses ke database %s", user, db_name);
        return;
    }

    if (access(path, F_OK) == -1) {
        snprintf(message, MAX_STR_LENGTH, "Table %s tidak ditemukan", table_name);
        return;
    }

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    char line[MAX_STR_LENGTH * MAX_COLUMNS];
    char newTable[MAX_STR_LENGTH * MAX_COLUMNS * MAX_ARR_SIZE];
    int linCount = 0;
    int deletedColumnPosition = -1;

    while (fgets(line, sizeof(line), file)) {
        if (linCount == 0) {
            char lineTemp[MAX_STR_LENGTH];
            strcpy(lineTemp, line);
            lineTemp[strcspn(lineTemp, "\n")] = '\0';
            char *token = strtok(lineTemp, " ");
            int count = 0;

            while (token != NULL) {
                char *tokenColumnName = removeType(token);
                if (strcmp(tokenColumnName, column_name) == 0) {
                    deletedColumnPosition = count;
                    break;
                }
                count++;
                token = strtok(NULL, " ");
            }
            if (deletedColumnPosition == -1) {
                snprintf(message, MAX_STR_LENGTH, "Kolom %s tidak ditemukan dalam table %s", column_name, table_name);
                fclose(file);
                return;
            }
        }

        int count = 0;

        char lineTemp[MAX_STR_LENGTH];
        strcpy(lineTemp, line);
        lineTemp[strcspn(lineTemp, "\n")] = '\0';
        char *token = strtok(lineTemp, " ");
        while (token != NULL) {
            if (count != deletedColumnPosition) {
                strcat(newTable, token);
                strcat(newTable, " ");
            }
            count++;
            token = strtok(NULL, " ");
        }
        strcat(newTable, "\n");
        linCount++;
    }

    fclose(file);
    FILE *output = fopen(path, "w");
    if (output == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    fputs(newTable, output);
    fclose(output);
    newTable[0] = '\0';

    snprintf(message, MAX_STR_LENGTH, "Kolom %s berhasil dihapus dari table %s", column_name, table_name);
}

void InsertInto(const char *db_name, const char *table_name, const char *insert_query) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Tulis data yang di-insert ke dalam tabel
    fprintf(file, "%s\n", insert_query);

    // Tutup file tabel
    fclose(file);
}

void InsertIntoTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    const char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Dapatkan query INSERT yang dikirim dari client
    comen = strtok(NULL, "");
    const char *insert_query = comen;

    // Panggil fungsi InsertInto untuk memasukkan data ke dalam tabel
    InsertInto(db_used, table_name, insert_query);

    sprintf(res_msg, "Data successfully inserted into table %s", table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void UpdateTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    // Ambil nama tabel
    comen = strtok(NULL, " ");
    char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom dan nilai yang ingin diupdate
    comen = strtok(NULL, " ");
    if (comen == NULL || strcmp(comen, "SET") != 0) {
        sprintf(res_msg, "Invalid UPDATE command syntax");
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom yang akan diupdate
    comen = strtok(NULL, "=");
    char *column_name = comen;

    // Ambil nilai baru yang akan dimasukkan ke dalam kolom
    comen = strtok(NULL, ";");
    char *new_value = comen;

    // Panggil fungsi Update untuk melakukan perubahan
    Update(db_used, table_name, column_name, new_value);

    sprintf(res_msg, "Column %s in table %s successfully updated", column_name, table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void Update(const char *db_name, const char *table_name, const char *column_name, const char *new_value) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Buka file sementara untuk menulis perubahan
    char temp_path[SMOL_BUF];
    snprintf(temp_path, sizeof(temp_path), "%s/%s/temp.txt", db_path, db_name);
    FILE *temp_file = fopen(temp_path, "w");
    if (temp_file == NULL) {
        printf("Error opening temporary file\n");
        fclose(file);
        return;
    }

    char line[BUFF_SIZE];
    while (fgets(line, sizeof(line), file)) {
        // Split baris menjadi kolom
        char *token = strtok(line, " ");
        if (token == NULL) {
            fprintf(temp_file, "%s", line);
            continue;
        }

        // Ambil nilai kolom pertama (sesuai dengan nama kolom yang ingin diupdate)
        if (strcmp(token, column_name) == 0) {
            // Ganti nilai kolom dengan nilai yang baru
            fprintf(temp_file, "%s %s\n", column_name, new_value);
        } else {
            fprintf(temp_file, "%s", line);
        }
    }

    // Tutup file
    fclose(file);
    fclose(temp_file);

    // Hapus file tabel lama
    remove(path);

    // Ubah nama file sementara menjadi nama file asli
    rename(temp_path, path);
}

//Melakukan logging
void writ_log(char *buffah) {
    char log_msg[BUFF_SIZE*2];
    char log_tim[SMOL_BUF];

    struct tm* this_tim;
    time_t teOne;
    teOne = time(NULL);
    this_tim = localtime(&teOne);
    strftime(log_tim, sizeof(log_tim), "[%Y-%m-%d %H:%M:%S]", this_tim);
    sprintf(log_msg, "%s:%s:%s", log_tim, logged_user, buffah);

    FILE *fp = fopen(db_log, "a");
    if(!fp) {
        printf("Failed to open the log file\n");
        return;
    }

    fprintf(fp, "%s\n", log_msg);
    fclose(fp);
}

//daemon ya daemon
void run_as_daemon() {
    pid_t pid;

    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Membuat socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed \n");
        exit(EXIT_FAILURE);
    }
    // Menetapkan informasi alamat server
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    // Binding socket ke alamat dan port tertentu
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }
    // Mendengarkan koneksi yang masuk
    if (listen(server_fd, MAX_PENDING_CONNECTIONS) < 0) {
        perror("Listen failed \n");
        exit(EXIT_FAILURE);
    }
    printf("Server running as daemon on port %d...\n", PORT);
    // Menjalankan sebagai daemon
    //run_as_daemon();
    // Menerima koneksi dan menangani permintaan
    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("Accept failed \n");
            exit(EXIT_FAILURE);
        }

        char buffer[1024] = {0};
        char cpy_buff[1024] = {0};
        int valread = recv(new_socket, buffer, 1024, 0);
        if (valread < 0) {
            perror("Error in receiving message from client \n");
            exit(EXIT_FAILURE);
        } else if (valread == 0) {
            printf("Client disconnected\n");
            close(new_socket);
            continue;
        }
        
        printf("Command received: %s\n", buffer);

        strcpy(cpy_buff, buffer); //salin buffer
        char *cmd_token = strtok(buffer, " "); //potong buffer

        if(strcmp(cmd_token, "LOGIN") == 0) db_login(new_socket, cmd_token);
        else if(strcmp(cmd_token, "CREATE") == 0) { 
            cmd_token = strtok(NULL, " ");

            if(strcmp(cmd_token, "USER") == 0) {
                if(arg_count(cpy_buff, 6)) mkusr(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "DATABASE") == 0) {
                if(arg_count(cpy_buff, 3)) CreateDatabase(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "TABLE") == 0) {
                if(!arg_count(cpy_buff, 3)) CreateTable(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "GRANT") == 0) {
            if(arg_count(cpy_buff, 5)) grnt_db(new_socket, cmd_token);
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "USE") == 0) {
            if(arg_count(cpy_buff, 2)) set_db(new_socket, cmd_token);
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "DROP") == 0) {
            cmd_token = strtok(NULL, " ");

            if(strcmp(cmd_token, "DATABASE") == 0) {
                if(arg_count(cpy_buff, 3)) DropDatabase(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "TABLE") == 0) {
                if(arg_count(cpy_buff, 3)) DropTable(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else invalid_cmd(new_socket);
        }
        else invalid_cmd(new_socket);
        close(new_socket); // Tutup socket untuk koneksi yang selesai
    }

    return 0;
}
```
**Penjelasan :**

```int arg_count(const char *argm, int target) {``` : Fungsi ini menghitung jumlah argumen khusus dalam string perintah yang diterima. Ini membantu dalam memvalidasi perintah yang diterima.

```void invalid_cmd(int new_socket) {``` : Fungsi ini mengirim pesan "Invalid Command!" ke soket client jika perintah yang diterima tidak valid

```void snd_msg(int new_socket, const char *msg_to_send) {``` : Fungsi ini mengirim pesan ke client melalui soket.

```int isDbExist(const char *db_name) {``` : Fungsi ini memeriksa apakah database ada dalam direktori databases.

```int isTableExist(const char *db_name, const char *table_name) {``` : Fungsi ini memeriksa apakah tabel ada dalam database yang diberikan. 

```int user_check(const char *uname, const char *passw) {``` : Fungsi ini memeriksa apakah pengguna terdaftar dan apakah kata sandinya cocok.

```int writ_usr(const char *new_uname, const char *new_passw) {``` : Fungsi ini menulis pengguna baru ke dalam file pengguna jika belum terdaftar sebelumnya.

```void db_login(int new_socket, char *comen) {``` : Fungsi ini mengelola proses login pengguna berdasarkan informasi yang diberikan.

```void mkusr(int new_socket, char *comen) {``` : Fungsi ini menangani pembuatan pengguna baru dengan memeriksa izin dan validitas data yang diterima.

```void grnt_db(int new_socket, char *comen) {``` : Fungsi ini memberikan izin akses database ke pengguna tertentu jika izin belum ada sebelumnya.

```void set_db(int new_socket, char *comen) {``` : Fungsi ini memungkinkan pengguna untuk menggunakan database tertentu.

# Hasil

  

## A. Autentikasi

  

Bagian Autentikasi merupakan bagian manajemen user dengan fungsi berupa login user dan register user.

Login user dilakukan secara bersamaan dengan saat menjalankan program `client` dengan cara sebagai berikut:
```
sudo ./client
```

untuk menjalankan program menggunakan user root yang dapat mengakses segala perintah dan database; dan juga
```
./client -u <USER> -p <PASSWORD>
```
untuk menjalankan program menggunakan user tertentu. User biasa hanya dapat mengakses database yang dibuat sendiri atau database lain yang diberi permission oleh root.

Registrasi atau `CREATE USER` hanya bisa dilakukan oleh root.

+ Fungsi login user (bagian client)
```c
//Autentikasi
    int sudo_kah = (geteuid() == 0);

    if(sudo_kah) { //Autentikasi root
        if(argc != 1) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN root root");

    } else { //Autentikasi user biasa
        char *username = NULL;
        char *passw = NULL;

        if(strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            to_use();
            return 1;
        }
        username = argv[2];
        passw = argv[4];

        if(username == NULL || passw == NULL || (strcmp(username, "root") == 0)) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN %s %s", username, passw);
        
    }
```

**Penjelasan :**

`int sudo_kah` merupakan variabel yang menerima result dari fungsi `getuid()`. Apabila fungsi bernilai nol, maka program dijalankan dengan user root dan variabel `sudo_kah` akan menerima nilai 1, apabila tidak maka akan bernilai 0.

Apabila variabel `sudo_kah` bernilai benar (1) maka program akan dilanjutkan dengan bagian autentikasi root yang mana client akan mengirim perintah login kepada database dalam bentuk `LOGIN root root`.

Apabila variabel bernilai salah (0) maka program akan dilanjutkan dengan bagian autentikasi user biasa yang mana client akan memeriksa argumen yang dijalankan setelah `./client`. Ketika argv[1] bernilai `-u` dan argv[3] bernilai `-p` maka autentikasi user lolos pada tahap validasi pertama. Selanjutnya akan diperiksa apakah argv[2] dan argv[4] memiliki nilai. Jika bernilai null, maka program akan menampilkan pesan tentang bagaimana program seharusnya dijalankan, Namun apabila benar, maka program akan mengirim pesan kepada database dengan bentuk `LOGIN <argv[2]> <argv[4]>`.

+ Fungsi login user (bagian database)

```c
//Fungsi login user
void db_login(int new_socket, char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    char *uname = comen;

    comen = strtok(NULL, " ");
    char *pass = comen;

    if(strcmp(uname, "root") == 0 && strcmp(pass, "root") == 0) {
        sprintf(res_msg, "logged in");
        printf("User logged in\n");
        strcpy(logged_user, uname);

    } else {
        int checkin = user_check(uname, pass);
        if(checkin == 0) {
            sprintf(res_msg, "logged in");
            printf("User logged in\n");
            strcpy(logged_user, uname);
        }
        else if(checkin > 0) sprintf(res_msg, "invalid username / password");
        else sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    }
    send(new_socket, res_msg, (strlen(res_msg)+1), 0);
}
```
**Penjelasan :**
  
  Pada bagian database, fungsi `db_login` akan mengatur semua yang berhubungan dengan login user. Ketika user dan password yang dikirim oleh client bernilai root, maka database akan langsung mengatur bahwa user yang menggunakan program saat itu adalah root. Namun apabila bukan, maka database akan memeriksa user dan password yang dikirim dari client.

Ketika user yang dimasukkan tidak terdaftar pada daftar user, atau password yang dimasukkan tidak sesuai dengan user yang dimasukkan, maka pesan akan mengirimkan pesan `invalid username/password`. Namun apabila berhasil, database akan mengatur bahwa user yang sedang menggunakan program tersebut adalah nama user yang melakukan login saat itu.

Hal ini diatur dari hasil return dari fungsi `user_check`. Ketika fungsi mengembalikan nilai -1, maka terjadi error saat program mencoba membuka daftar user, apabila bernilai 0 maka user berhasil login, dan apabila bernilai 1 maka user atau password yang dimasukkan tidak valid.

+ Fungsi create user
```c
void mkusr(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to create new user, access denied\n", logged_user);
        char *server_message = "You dont have any permissions to do that!";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    char *uname_new = comen;
    if(strcmp(uname_new, "root") == 0) {
        char *server_message = "User name is not allowed";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "IDENTIFIED") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "BY") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *passw_new = comen;

    int checkin = writ_usr(uname_new, passw_new);

    if(checkin < 0) sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    else if(checkin > 0) sprintf(res_msg, "Username %s is already taken", uname_new);
    else if(checkin == 0) sprintf(res_msg, "Added username %s", uname_new);

    send(new_socket, res_msg, strlen(res_msg), 0);
}
```
 Fungsi `mkusr` akan mengatur semua hal yang berkaitan dengan `CREATE USER`.
 
Sebelum dilakukan pembuatan user, dilakukan pengecekan user terlebih dahulu.

Apabila user yang mengirimkan pesan tersebut adalah root, maka program akan berlanjut, apabila tidak program akan mengirimkan pesan eroor pada client.

Program kemudian akan memeriksa apakah nama user yang dibuat bernama `root`, apabila iya maka program akan mngirimkan pesan error karena username root sudah ada secara default dan tidak dapat dibuat lagi.

Selanjutnya program akan memeriksa apakah argumen yang digunakan valid serta memeriksa apakah password yang dimasukkan tidak bernilai NULL.

Program akhirnya akan menjalankan fungsi penulisan user dan password yang akan mengembalikan nilai kode integer.

 - Kode -1: error saat mengakses file daftar user
 - Kode 0: berhasil menuliskan user baru
 - Kode 1: user yang akan ditambahkan sudah terdaftar

## B. Autorisasi

Bagian autorisasi merupakan bagian untuk mengatur penggunaan database dan permission dari setiap database. Bagian ini terbagi menjadi 2 fungsi, yaitu `USE database` dan `GRANT PERMISSION`

+ Fungsi `USE database`
```c
//Fungsi untuk menggunakan database
void set_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    comen = strtok(NULL, " ");
    char *db_target = comen;

    int stawtus = find_db_perms(db_target, logged_user);
    if(stawtus == 0) {
        sprintf(res_msg, "User %s dont have permission to use %s database", logged_user, db_target);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    strcpy(db_used, db_target);
    sprintf(res_msg, "You are now using %s database", db_target);
    snd_msg(new_socket, res_msg);
    return;
}
```

**Penjelasan :**

Fungsi untuk menggunakan database semuanya diatur pada fungsi `set_db`. Bagian ini akan memeriksa terlebih dahulu apakah user yang login memiliki permission untuk menggunakan database tujuan melalui fungsi `find_db_perms` yang mengembalikan nilai berupa kode.

 - Kode -2: Database yang ingin digunakan tidak ada
 - Kode -1: Terjadi error saat mencari database
 - Kode 0: User tidak memiliki permission
 - Selain itu: user dapat menggunakan database
 
Setelah melalui fungsi pencarian dan user diketahui memiliki akses ke databse tujuan, program akan mengatur agar databse yang digunakan saat ini adalah database tujuan yang diinginkan oleh user.

 
 + Fungsi `GRANT PERMISSION`
 
```c
//Fungsi Grant Permissions db
void grnt_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "PERMISSION") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *db_check = comen;

    if(strcmp(db_check, "user_db") == 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "INTO") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *to_user = comen;

    int stawtus = user_exist(to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s is not registered", to_user);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the user data file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    stawtus = 0;

    stawtus = find_db_perms(db_check, to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s already have permissions to %s database", to_user, db_check);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_check, db_check);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "%s\n", to_user);
    fclose(fp);

    sprintf(res_msg, "User %s has been granted permission to %s database", to_user, db_check);
    snd_msg(new_socket, res_msg);
    return;
}
```
**Penjelasan:**

Fungsi untuk memberikan akses database semuanya diatur melalui fungsi `grnt_db`.

Karena yang mampu memberikan akses database hanyalah root, maka program pertama-tama akan memeriksa apakah user yang saat ini login merupakan root, apabila tidak maka program akan mengirimkan pesan error.

Setelah itu, program akan memeriksa apakah user yang diinputkan ada atau tidak melalui fungsi `user_exist`.

Program kemudian akan memeriksa apakah user yang diinputkan telah memiliki akses ke database tujuan melalui fungsi `find_db_perms` yang akan mengembalikan nilai berupa kode.

 - Kode -2: database tidak ada
 - Kode -1: Terjadi error saat mencari database
 - Kode 1: User sudah memiliki akses pada database
 - Selain itu: user belum memiliki akses ke database tujuan.
 
 program kemudian akan menuliskan dama user ke dalam file permission yang dimiliki oleh database tersebut.

## C. Data Definition Language

```c
//Fungsi membuat database
void CreateDatabase(int new_socket, const char *comen) {
    char res_msg[SMOL_BUF] = {0};
    comen = strtok(NULL, " ");

    char *db_name = comen;
    if(isDbExist(db_name)) {
        sprintf(res_msg, "Database %s is already exist!", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    char command[BUFF_SIZE + strlen(db_name) + 100];
    snprintf(command, sizeof(command), "mkdir -p %s/%s",db_path ,db_name);
    int status = system(command);

    if (status != 0) {
        strcpy(res_msg, "Gagal create database");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_name, db_name);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "permissions string\n");
    if(strcmp(logged_user, "root") == 0) fprintf(fp, "root\n");
    else fprintf(fp, "root\n%s\n", logged_user);
    fclose(fp);

    strcpy(res_msg, "Berhasil create database");
    snd_msg(new_socket, res_msg);
    printf("%s\n", res_msg);
}

//Fungsi membuat tabel
void CreateTable(int new_socket, const char *comen) {
    char path[SMOL_BUF];
    char res_msg[BUFF_SIZE];
    snprintf(path, sizeof(path), "%s/%s",db_path, db_used);

    comen = strtok(NULL, " ");
    char *table_name = comen;
    if(isTableExist(db_used, table_name) == 1) {
        sprintf(res_msg, "Table %s is already exist!", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE];
    char columnData[BUFF_SIZE * MAX_COLUMNS * 20];
    char dest[BUFF_SIZE];

    snprintf(command, sizeof(command), "touch %s/%s.txt", path, table_name);
    system(command);

    comen = strtok(NULL, " ");
    char *query = comen;

    char *start = strchr(query, '(') + 1;
    char *end = strchr(query, ')');
    if (end != NULL) *end = '\0';

    struct Column cols[MAX_COLUMNS];
    int numCols = 0;

    char *token = strtok(start, " ");
    while (token != NULL) {
        snprintf(cols[numCols].column, sizeof(cols[numCols].column), "%s", token);
        token = strtok(NULL, ",");
        if (token != NULL) {
            snprintf(cols[numCols].datatype, sizeof(cols[numCols].datatype), "%s", token);
            token = strtok(NULL, " ");
        }
        numCols++;
    }

    command[0] = '\0';
    snprintf(command, sizeof(command), "echo ");

    for (int i = 0; i < numCols; ++i) {
        snprintf(columnData, sizeof(columnData), "%s %s;", cols[i].column, cols[i].datatype);
        strcat(command, columnData);
        if (i != numCols - 1) {
            strcat(command, " ");
        }
    }

    snprintf(dest, sizeof(dest), " > %s/%s.txt", path, table_name);
    strcat(command, dest);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Table %s created in Database %s", table_name, db_used);
    printf("Table %s created in Database %s\n", table_name, db_used);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus database
void DropDatabase(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *db_name = comen;
    char *end = strchr(db_name, ';');   
    if (end != NULL) *end = '\0';

    if(isDbExist(db_name) == 0) {
        sprintf(res_msg, "%s is not exist", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    if(find_db_perms(db_name, logged_user) == 0) {
        sprintf(res_msg, "%s have no permission to %s database", logged_user, db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    
    char command[MAX_STR_LENGTH * 2];
    snprintf(command, sizeof(command), "rm -rf %s/%s", db_path, db_name);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Database %s berhasil dihapus", db_name);
    printf("%s\n", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus table berdasarkan database yang sedang digunakan
void DropTable(int new_socket, const char *comen) {
    char path[BUFF_SIZE];
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *table_name = comen;
    char *end = strchr(table_name, ';');   
    if (end != NULL) *end = '\0';

    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_used, table_name);

    if(isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s is not exist in current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE + 100];
    snprintf(command, sizeof(command), "rm -r %s", path);
    system(command);

    snprintf(res_msg, BUFF_SIZE, "Table %s berhasil dihapus", table_name);
    printf("%s", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

void DropColumn(const char *db_name, const char *table_name, const char *column_name, int is_sudo, const char *user) {
    char path[MAX_STR_LENGTH];
    char message[BUFF_SIZE];
    snprintf(path, sizeof(path), "/home/tzubast/FP\\ SISOP/database/databases/%s/%s.txt", db_name, table_name);

    if (!is_sudo && strcmp(user, "admin") != 0) {
        snprintf(message, MAX_STR_LENGTH, "User %s tidak memiliki akses ke database %s", user, db_name);
        return;
    }

    if (access(path, F_OK) == -1) {
        snprintf(message, MAX_STR_LENGTH, "Table %s tidak ditemukan", table_name);
        return;
    }

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    char line[MAX_STR_LENGTH * MAX_COLUMNS];
    char newTable[MAX_STR_LENGTH * MAX_COLUMNS * MAX_ARR_SIZE];
    int linCount = 0;
    int deletedColumnPosition = -1;

    while (fgets(line, sizeof(line), file)) {
        if (linCount == 0) {
            char lineTemp[MAX_STR_LENGTH];
            strcpy(lineTemp, line);
            lineTemp[strcspn(lineTemp, "\n")] = '\0';
            char *token = strtok(lineTemp, " ");
            int count = 0;

            while (token != NULL) {
                char *tokenColumnName = removeType(token);
                if (strcmp(tokenColumnName, column_name) == 0) {
                    deletedColumnPosition = count;
                    break;
                }
                count++;
                token = strtok(NULL, " ");
            }
            if (deletedColumnPosition == -1) {
                snprintf(message, MAX_STR_LENGTH, "Kolom %s tidak ditemukan dalam table %s", column_name, table_name);
                fclose(file);
                return;
            }
        }

        int count = 0;

        char lineTemp[MAX_STR_LENGTH];
        strcpy(lineTemp, line);
        lineTemp[strcspn(lineTemp, "\n")] = '\0';
        char *token = strtok(lineTemp, " ");
        while (token != NULL) {
            if (count != deletedColumnPosition) {
                strcat(newTable, token);
                strcat(newTable, " ");
            }
            count++;
            token = strtok(NULL, " ");
        }
        strcat(newTable, "\n");
        linCount++;
    }

    fclose(file);
    FILE *output = fopen(path, "w");
    if (output == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    fputs(newTable, output);
    fclose(output);
    newTable[0] = '\0';

    snprintf(message, MAX_STR_LENGTH, "Kolom %s berhasil dihapus dari table %s", column_name, table_name);
}
```
**Penjelasan :**

```void CreateDatabase(int new_socket, const char *comen) {``` : Fungsi ini membuat database baru jika belum ada sebelumnya.

```void CreateTable(int new_socket, const char *comen) {``` : Fungsi ini membuat tabel baru dalam database yang digunakan.

```void DropDatabase(int new_socket, const char *comen) {``` : Fungsi ini menghapus database jika ada izin untuk melakukannya.

```void DropTable(int new_socket, const char *comen) {``` : Fungsi ini menghapus tabel dalam database yang digunakan jika izin diberikan.
 :

## D. Data Manipulation Laguage

```c
void InsertInto(const char *db_name, const char *table_name, const char *insert_query) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Tulis data yang di-insert ke dalam tabel
    fprintf(file, "%s\n", insert_query);

    // Tutup file tabel
    fclose(file);
}
void InsertIntoTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    const char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Dapatkan query INSERT yang dikirim dari client
    comen = strtok(NULL, "");
    const char *insert_query = comen;

    // Panggil fungsi InsertInto untuk memasukkan data ke dalam tabel
    InsertInto(db_used, table_name, insert_query);

    sprintf(res_msg, "Data successfully inserted into table %s", table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void UpdateTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    // Ambil nama tabel
    comen = strtok(NULL, " ");
    char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom dan nilai yang ingin diupdate
    comen = strtok(NULL, " ");
    if (comen == NULL || strcmp(comen, "SET") != 0) {
        sprintf(res_msg, "Invalid UPDATE command syntax");
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom yang akan diupdate
    comen = strtok(NULL, "=");
    char *column_name = comen;

    // Ambil nilai baru yang akan dimasukkan ke dalam kolom
    comen = strtok(NULL, ";");
    char *new_value = comen;

    // Panggil fungsi Update untuk melakukan perubahan
    Update(db_used, table_name, column_name, new_value);

    sprintf(res_msg, "Column %s in table %s successfully updated", column_name, table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void Update(const char *db_name, const char *table_name, const char *column_name, const char *new_value) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Buka file sementara untuk menulis perubahan
    char temp_path[SMOL_BUF];
    snprintf(temp_path, sizeof(temp_path), "%s/%s/temp.txt", db_path, db_name);
    FILE *temp_file = fopen(temp_path, "w");
    if (temp_file == NULL) {
        printf("Error opening temporary file\n");
        fclose(file);
        return;
    }

    char line[BUFF_SIZE];
    while (fgets(line, sizeof(line), file)) {
        // Split baris menjadi kolom
        char *token = strtok(line, " ");
        if (token == NULL) {
            fprintf(temp_file, "%s", line);
            continue;
        }

        // Ambil nilai kolom pertama (sesuai dengan nama kolom yang ingin diupdate)
        if (strcmp(token, column_name) == 0) {
            // Ganti nilai kolom dengan nilai yang baru
            fprintf(temp_file, "%s %s\n", column_name, new_value);
        } else {
            fprintf(temp_file, "%s", line);
        }
    }
```
**Penjelasan :**

```void InsertIntoTable(int new_socket, const char *comen) {``` : Fungsi ini memasukkan data ke dalam tabel yang sudah ada dalam database yang digunakan.

```void UpdateTable(int new_socket, const char *comen) {``` : Fungsi ini mengupdate nilai kolom dalam tabel yang digunakan.

```void Update(const char *db_name, const char *table_name, const char *column_name, const char *new_value) {``` : Fungsi ini mengupdate nilai kolom dalam tabel yang diberikan di dalam database.`

## E. Logging

```c
//Melakukan logging
void writ_log(char *buffah) {
    char log_msg[BUFF_SIZE*2];
    char log_tim[SMOL_BUF];

    struct tm* this_tim;
    time_t teOne;
    teOne = time(NULL);
    this_tim = localtime(&teOne);
    strftime(log_tim, sizeof(log_tim), "[%Y-%m-%d %H:%M:%S]", this_tim);
    sprintf(log_msg, "%s:%s:%s", log_tim, logged_user, buffah);

    FILE *fp = fopen(db_log, "a");
    if(!fp) {
        printf("Failed to open the log file\n");
        return;
    }

    fprintf(fp, "%s\n", log_msg);
    fclose(fp);
}
```
**Penjelasan :**

Fungsi logging ini diletakkan pada bagian main setelah database menerima perintah dari client. Fungsi ini akan mencatat setiap perintah yang dikirim user baik itu valid maupun tidak.


## H. Error Handling

Error handling yang kami gunakan di sini disederhanakan melalui dua fungsi yang akan mempermudah dalam pengiriman pesan.
```c
//Tampilkan error tanpa harus keluar dari program
void invalid_cmd(int new_socket) {
    char *server_message = "Invalid Command!";
    printf("Send message: %s\n", server_message);
    send(new_socket, server_message, strlen(server_message), 0);
}

//Kirim pesan instan ke client
void snd_msg(int new_socket, const char *msg_to_send) {
    printf("Send message: %s\n", msg_to_send);
    send(new_socket, msg_to_send, strlen(msg_to_send), 0);
}
```
**Penjelasan :**

Fungsi `invalid_cmd` merupakan fungsi yang akan mengirimkan pesan error default ke bagian cilent setiap kali ada perintah yang tidak sesuai dengan aturan. Fungsi ini dipasang di setiap fungsi lain yang menerima perintah dari client.

Setiap kali ada penggunaan perintah yang tidak sesuai aturan, fungsi yang bersangkutan akan menjalankan fungsi ini dan menghentikan fungsi mereka sendiri, sehingga program tidak perlu berhenti dan user dapat mengirim ulang perintah yang dimaksudkan.

Sementara fungsi `snd_msg` merupakan fungsi universal untuk menyederhanakan pengiriman pesan ke pihak client. Hal ini dapat menyederhanakan pengiriman pesan baik itu pesan sukses maupun pesan yang berisi custom error message.

# Kesimpulan

Kesimpulan yang dapat IT-12 sudah bisa membuat program client yang bisa terhubung dengan program database melalui socket. program client sendiri mengirimkan perintah ke program database yang dapat melakukan autentikasi user, autorisasi, Data Definition Language, Data Manipulation Language, Logging, Reliability dan Error handling. Kekurangan dari program ini yaitu belum bisa melakukan delete dan update column.




