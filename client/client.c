#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#define PORT 8080
#define BUFFER_SIZE 1024

void to_use() {
    printf("To use this database program:\n");
    printf("> Run as Root: sudo ./client\n");
    printf("> Login as user: ./client -u <username> -p <password>\n");
    printf("Username \"Admin\" is not allowed\n");
}

int mksock() {
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) return -1;

    // Menetapkan informasi alamat server
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Konversi alamat IP dari string ke format biner
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) return -2;

    // Melakukan koneksi ke server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) return -3;

    return sock;
}

//menerima dan mengirim program database
void prog_run() {
    while(1) {
        char cmdreq[BUFFER_SIZE];
        char buffer[BUFFER_SIZE] = {0};
        int sock = 0;
        int status_cek=0;

        //Input Message
        printf("> ");
        fgets(cmdreq, BUFFER_SIZE, stdin);
        cmdreq[strcspn(cmdreq, "\n")] = '\0';

        // Membuat socket
        sock = mksock();
        if(sock < 0) {
            printf("An error has occured while creating socket, please try again. Error(%d)\n", sock);
            continue;
        }

        //Mengirim command ke server
        status_cek = send(sock, cmdreq, strlen(cmdreq), 0);
        if(status_cek == -1) {
            perror("Failed to send command\n");
            close(sock);
            continue;
        }
        status_cek = 0;

        //Output dari server database
        status_cek = read(sock, buffer, BUFFER_SIZE);
        if(status_cek == -1) {
            perror("Failed to read output\n");
            close(sock);
            break;
        }
        printf("%s, %d\n", buffer, (int)strlen(buffer));
        if(strcmp(buffer, "Turning off database") == 0) break;

        close(sock);
    }
}

int main(int argc, char *argv[]) {
    char *loginreq = malloc(BUFFER_SIZE);
    char buffer[BUFFER_SIZE];

    //Autentikasi
    int sudo_kah = (geteuid() == 0);

    if(sudo_kah) { //Autentikasi root
        if(argc != 1) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN root root");

    } else { //Autentikasi user biasa
        char *username = NULL;
        char *passw = NULL;

        if(strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            to_use();
            return 1;
        }
        username = argv[2];
        passw = argv[4];

        if(username == NULL || passw == NULL || (strcmp(username, "root") == 0)) {
            to_use();
            return 1;
        }
        sprintf(loginreq, "LOGIN %s %s", username, passw);
        
    }

    int sock = 0;
    struct sockaddr_in serv_addr;

    // Membuat socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    // Menetapkan informasi alamat server
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Konversi alamat IP dari string ke format biner
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    // Melakukan koneksi ke server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed\n");
        exit(EXIT_FAILURE);
    }

    //mengirim izin login
    send(sock, loginreq, strlen(loginreq), 0);
    free(loginreq);

    // Menerima respon dari server
    recv(sock, buffer, BUFFER_SIZE, 0);
    close(sock);

    if(strcmp(buffer, "logged in") == 0) {

        //jika berhasil, jalankan program database
        printf("Welcome to database\n");
        prog_run();

    } else {
        printf("%s\n", buffer);
        return 1;
    }
    return 0;
}
