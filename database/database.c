#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <ctype.h>

#define PORT 8080
#define MAX_PENDING_CONNECTIONS 10
#define BUFF_SIZE 1024
#define SMOL_BUF 256
#define MAX_SIZE 100
#define MAX_STR_LENGTH 100
#define MAX_LENGTH 100
#define MAX_ROWS 100
#define MAX_ARR_SIZE 100
#define MAX_USERS 10
#define MAX_COLUMNS 10

const char *db_path = "databases/";
const char *db_users = "databases/user_db/users.txt";

char logged_user[32];
char db_used[32];

struct Column {
    char *column;
    char *datatype;
};

//Menghitung jumlah custom argumen DB
int arg_count(const char *argm, int target) {
    int count = 0;
    for(int i=0; i<strlen(argm); i++) {
        if(argm[i] == ' ') count++;
    }
    count++;
    if(count != target) return 0; //nol berarti fek
    return 1; //satu berarti rill
}

//Tampilkan error tanpa harus keluar dari program
void invalid_cmd(int new_socket) {
    char *server_message = "Invalid Command!";
    printf("Send message: %s\n", server_message);
    send(new_socket, server_message, strlen(server_message), 0);
}

//Kirim pesan instan ke client
void snd_msg(int new_socket, const char *msg_to_send) {
    printf("Send message: %s\n", msg_to_send);
    send(new_socket, msg_to_send, strlen(msg_to_send), 0);
}

//Cek database ada apa nda
int isDbExist(const char *db_name) {
    char db_ril_path[SMOL_BUF];
    sprintf(db_ril_path, "%s/%s", db_path, db_name);
    struct stat stats;

    stat(db_ril_path, &stats);
    if (S_ISDIR(stats.st_mode)) return 1;
    return 0;
}

//Cek table ada apa nda
int isTableExist(const char *db_name, const char *table_name) {
    char table_ril_path[SMOL_BUF];
    sprintf(table_ril_path, "%s/%s/%s.txt", db_path, db_name, table_name);

    if(access(table_ril_path, F_OK)) return 1;
    return 0;
}

//Periksa apakah user ada di daftar user dan apakah passwordnya sesuai
int user_check(const char *uname, const char *passw) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        usr_token = strtok(NULL, ";");
        char *temp_passw = usr_token;

        if(strcmp(uname, temp_uname) == 0 && strcmp(passw, temp_passw) == 0) {
            fclose(fp);
            return 0;
        }
    }

    fclose(fp);
    return 1;
}

//Periksa apakah user terdaftar atau tidak
int user_exist(const char *uname) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        if(strcmp(uname, temp_uname) == 0) {
            fclose(fp);
            return 0;
        }
    }

    fclose(fp);
    return 1;
}

//Fungsi login user
void db_login(int new_socket, char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    char *uname = comen;

    comen = strtok(NULL, " ");
    char *pass = comen;

    if(strcmp(uname, "root") == 0 && strcmp(pass, "root") == 0) {
        sprintf(res_msg, "logged in");
        printf("User logged in\n");
        strcpy(logged_user, uname);

    } else {
        int checkin = user_check(uname, pass);
        if(checkin == 0) {
            sprintf(res_msg, "logged in");
            printf("User logged in\n");
            strcpy(logged_user, uname);
        }
        else if(checkin > 0) sprintf(res_msg, "invalid username / password");
        else sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    }
    send(new_socket, res_msg, (strlen(res_msg)+1), 0);
}

//Fungsi tulis user baru
int writ_usr(const char *new_uname, const char *new_passw) {
    char newline[SMOL_BUF];
    FILE *fp = fopen(db_users, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';

        char *usr_token = strtok(newline, ";");
        char *temp_uname = usr_token;

        if(strcmp(new_uname, temp_uname) == 0) {
            fclose(fp);
            return 1;
        }
    }
    fclose(fp);

    FILE *nfp = fopen(db_users, "a");
    if(!nfp) return -1;
    fprintf(nfp, "%s;%s\n", new_uname, new_passw);
    fclose(nfp);
    return 0;
}

//Fungsi bikin user
void mkusr(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to create new user, access denied\n", logged_user);
        char *server_message = "You dont have any permissions to do that!";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    char *uname_new = comen;
    if(strcmp(uname_new, "root") == 0) {
        char *server_message = "User name is not allowed";
        send(new_socket, server_message, strlen(server_message), 0);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "IDENTIFIED") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "BY") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *passw_new = comen;

    int checkin = writ_usr(uname_new, passw_new);

    if(checkin < 0) sprintf(res_msg, "an error has occured, cant access users database (error %d)", checkin);
    else if(checkin > 0) sprintf(res_msg, "Username %s is already taken", uname_new);
    else if(checkin == 0) sprintf(res_msg, "Added username %s", uname_new);

    send(new_socket, res_msg, strlen(res_msg), 0);
}

//Fungsi cari permissions user
int find_db_perms(const char *db_name, const char *u_name) {
    char access_buf[SMOL_BUF] = {0};
    char newline[SMOL_BUF];
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_name, db_name);

    if(access(access_buf, F_OK)) return -2; //cek apakah file perms ada, jika ada maka db juga ada

    FILE *fp = fopen(access_buf, "r");
    if(!fp) return -1;

    while(fgets(newline, SMOL_BUF, fp) != NULL) {
        newline[strcspn(newline, "\n")] = '\0';
        if(strcmp(newline, u_name) == 0) {
            fclose(fp);
            return 1;
        }
    }
    fclose(fp);
    return 0;
}

//Fungsi Grant Permissions db
void grnt_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    if(strcmp(logged_user, "root") != 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "PERMISSION") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *db_check = comen;

    if(strcmp(db_check, "user_db") == 0) {
        printf("> %s is trying to use GRANT command, access denied\n", logged_user);
        sprintf(res_msg, "You dont have any permissions to do that!");
        snd_msg(new_socket, res_msg);
        return;
    }

    comen = strtok(NULL, " ");
    if(strcmp(comen, "INTO") != 0) {
        invalid_cmd(new_socket);
        return;
    }

    comen = strtok(NULL, " ");
    char *to_user = comen;

    int stawtus = user_exist(to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s is not registered", to_user);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the user data file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    stawtus = 0;

    stawtus = find_db_perms(db_check, to_user);
    if(stawtus > 0) {
        sprintf(res_msg, "User %s already have permissions to %s database", to_user, db_check);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_check, db_check);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "%s\n", to_user);
    fclose(fp);

    sprintf(res_msg, "User %s has been granted permission to %s database", to_user, db_check);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi untuk menggunakan database
void set_db(int new_socket, char *comen) {
    char res_msg[SMOL_BUF] = {0};

    comen = strtok(NULL, " ");
    char *db_target = comen;

    int stawtus = find_db_perms(db_target, logged_user);
    if(stawtus == 0) {
        sprintf(res_msg, "User %s dont have permission to use %s database", logged_user, db_target);
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -1) {
        sprintf(res_msg, "An error occured while accessing the perms file, please try again");
        snd_msg(new_socket, res_msg);
        return;
    }
    else if(stawtus == -2) {
        sprintf(res_msg, "The database is not exist");
        snd_msg(new_socket, res_msg);
        return;
    }

    strcpy(db_used, db_target);
    sprintf(res_msg, "You are now using %s database", db_target);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi membuat database
void CreateDatabase(int new_socket, const char *comen) {
    char res_msg[SMOL_BUF] = {0};
    comen = strtok(NULL, " ");

    char *db_name = comen;
    if(isDbExist(db_name)) {
        sprintf(res_msg, "Database %s is already exist!", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    char command[BUFF_SIZE + strlen(db_name) + 100];
    snprintf(command, sizeof(command), "mkdir -p %s/%s",db_path ,db_name);
    int status = system(command);

    if (status != 0) {
        strcpy(res_msg, "Gagal create database");
        snd_msg(new_socket, res_msg);
        return;
    }

    char access_buf[SMOL_BUF] = {0};
    sprintf(access_buf, "%s/%s/%s_perms.txt", db_path, db_name, db_name);
    FILE *fp = fopen(access_buf, "a");
    if(!fp) {
        sprintf(res_msg, "An error has occured while opening access file, please check if everything is alright");
        snd_msg(new_socket, res_msg);
        return;
    }

    fprintf(fp, "permissions string\n");
    if(strcmp(logged_user, "root") == 0) fprintf(fp, "root\n");
    else fprintf(fp, "root\n%s\n", logged_user);
    fclose(fp);

    strcpy(res_msg, "Berhasil create database");
    snd_msg(new_socket, res_msg);
    printf("%s\n", res_msg);
}

//Fungsi membuat tabel
void CreateTable(int new_socket, const char *comen) {
    char path[SMOL_BUF];
    char res_msg[BUFF_SIZE];
    snprintf(path, sizeof(path), "%s/%s",db_path, db_used);

    comen = strtok(NULL, " ");
    char *table_name = comen;
    if(isTableExist(db_used, table_name) == 1) {
        sprintf(res_msg, "Table %s is already exist!", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE];
    char columnData[BUFF_SIZE * MAX_COLUMNS * 20];
    char dest[BUFF_SIZE];

    snprintf(command, sizeof(command), "touch %s/%s.txt", path, table_name);
    system(command);

    comen = strtok(NULL, " ");
    char *query = comen;

    char *start = strchr(query, '(') + 1;
    char *end = strchr(query, ')');
    if (end != NULL) *end = '\0';

    struct Column cols[MAX_COLUMNS];
    int numCols = 0;

    char *token = strtok(start, " ");
    while (token != NULL) {
        snprintf(cols[numCols].column, sizeof(cols[numCols].column), "%s", token);
        token = strtok(NULL, ",");
        if (token != NULL) {
            snprintf(cols[numCols].datatype, sizeof(cols[numCols].datatype), "%s", token);
            token = strtok(NULL, " ");
        }
        numCols++;
    }

    command[0] = '\0';
    snprintf(command, sizeof(command), "echo ");

    for (int i = 0; i < numCols; ++i) {
        snprintf(columnData, sizeof(columnData), "%s %s;", cols[i].column, cols[i].datatype);
        strcat(command, columnData);
        if (i != numCols - 1) {
            strcat(command, " ");
        }
    }

    snprintf(dest, sizeof(dest), " > %s/%s.txt", path, table_name);
    strcat(command, dest);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Table %s created in Database %s", table_name, db_used);
    printf("Table %s created in Database %s\n", table_name, db_used);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus database
void DropDatabase(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *db_name = comen;
    char *end = strchr(db_name, ';');   
    if (end != NULL) *end = '\0';

    if(isDbExist(db_name) == 0) {
        sprintf(res_msg, "%s is not exist", db_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    if(find_db_perms(db_name, logged_user) == 0) {
        sprintf(res_msg, "%s have no permission to %s database", logged_user, db_name);
        snd_msg(new_socket, res_msg);
        return;
    }
    
    char command[MAX_STR_LENGTH * 2];
    snprintf(command, sizeof(command), "rm -rf %s/%s", db_path, db_name);
    system(command);

    snprintf(res_msg, MAX_STR_LENGTH, "Database %s berhasil dihapus", db_name);
    printf("%s\n", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

//Fungsi hapus table berdasarkan database yang sedang digunakan
void DropTable(int new_socket, const char *comen) {
    char path[BUFF_SIZE];
    char res_msg[BUFF_SIZE];

    comen = strtok(NULL, " ");
    char *table_name = comen;
    char *end = strchr(table_name, ';');   
    if (end != NULL) *end = '\0';

    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_used, table_name);

    if(isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s is not exist in current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    char command[BUFF_SIZE + 100];
    snprintf(command, sizeof(command), "rm -r %s", path);
    system(command);

    snprintf(res_msg, BUFF_SIZE, "Table %s berhasil dihapus", table_name);
    printf("%s", res_msg);
    snd_msg(new_socket, res_msg);
    return;
}

void DropColumn(const char *db_name, const char *table_name, const char *column_name, int is_sudo, const char *user) {
    char path[MAX_STR_LENGTH];
    char message[BUFF_SIZE];
    snprintf(path, sizeof(path), "/home/tzubast/FP\\ SISOP/database/databases/%s/%s.txt", db_name, table_name);

    if (!is_sudo && strcmp(user, "admin") != 0) {
        snprintf(message, MAX_STR_LENGTH, "User %s tidak memiliki akses ke database %s", user, db_name);
        return;
    }

    if (access(path, F_OK) == -1) {
        snprintf(message, MAX_STR_LENGTH, "Table %s tidak ditemukan", table_name);
        return;
    }

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    char line[MAX_STR_LENGTH * MAX_COLUMNS];
    char newTable[MAX_STR_LENGTH * MAX_COLUMNS * MAX_ARR_SIZE];
    int linCount = 0;
    int deletedColumnPosition = -1;

    while (fgets(line, sizeof(line), file)) {
        if (linCount == 0) {
            char lineTemp[MAX_STR_LENGTH];
            strcpy(lineTemp, line);
            lineTemp[strcspn(lineTemp, "\n")] = '\0';
            char *token = strtok(lineTemp, " ");
            int count = 0;

            while (token != NULL) {
                char *tokenColumnName = removeType(token);
                if (strcmp(tokenColumnName, column_name) == 0) {
                    deletedColumnPosition = count;
                    break;
                }
                count++;
                token = strtok(NULL, " ");
            }
            if (deletedColumnPosition == -1) {
                snprintf(message, MAX_STR_LENGTH, "Kolom %s tidak ditemukan dalam table %s", column_name, table_name);
                fclose(file);
                return;
            }
        }

        int count = 0;

        char lineTemp[MAX_STR_LENGTH];
        strcpy(lineTemp, line);
        lineTemp[strcspn(lineTemp, "\n")] = '\0';
        char *token = strtok(lineTemp, " ");
        while (token != NULL) {
            if (count != deletedColumnPosition) {
                strcat(newTable, token);
                strcat(newTable, " ");
            }
            count++;
            token = strtok(NULL, " ");
        }
        strcat(newTable, "\n");
        linCount++;
    }

    fclose(file);
    FILE *output = fopen(path, "w");
    if (output == NULL) {
        snprintf(message, MAX_STR_LENGTH, "Gagal membuka file.");
        return;
    }

    fputs(newTable, output);
    fclose(output);
    newTable[0] = '\0';

    snprintf(message, MAX_STR_LENGTH, "Kolom %s berhasil dihapus dari table %s", column_name, table_name);
}

void InsertInto(const char *db_name, const char *table_name, const char *insert_query) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "a");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Tulis data yang di-insert ke dalam tabel
    fprintf(file, "%s\n", insert_query);

    // Tutup file tabel
    fclose(file);
}

void InsertIntoTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    comen = strtok(NULL, " ");
    const char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Dapatkan query INSERT yang dikirim dari client
    comen = strtok(NULL, "");
    const char *insert_query = comen;

    // Panggil fungsi InsertInto untuk memasukkan data ke dalam tabel
    InsertInto(db_used, table_name, insert_query);

    sprintf(res_msg, "Data successfully inserted into table %s", table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void UpdateTable(int new_socket, const char *comen) {
    char res_msg[BUFF_SIZE] = {0};

    // Ambil nama tabel
    comen = strtok(NULL, " ");
    char *table_name = comen;

    // Lakukan pengecekan apakah tabel ada
    if (!isTableExist(db_used, table_name)) {
        sprintf(res_msg, "Table %s does not exist in the current database", table_name);
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom dan nilai yang ingin diupdate
    comen = strtok(NULL, " ");
    if (comen == NULL || strcmp(comen, "SET") != 0) {
        sprintf(res_msg, "Invalid UPDATE command syntax");
        snd_msg(new_socket, res_msg);
        return;
    }

    // Ambil nama kolom yang akan diupdate
    comen = strtok(NULL, "=");
    char *column_name = comen;

    // Ambil nilai baru yang akan dimasukkan ke dalam kolom
    comen = strtok(NULL, ";");
    char *new_value = comen;

    // Panggil fungsi Update untuk melakukan perubahan
    Update(db_used, table_name, column_name, new_value);

    sprintf(res_msg, "Column %s in table %s successfully updated", column_name, table_name);
    snd_msg(new_socket, res_msg);
}

// Fungsi untuk melakukan update pada tabel
void Update(const char *db_name, const char *table_name, const char *column_name, const char *new_value) {
    char path[SMOL_BUF];
    snprintf(path, sizeof(path), "%s/%s/%s.txt", db_path, db_name, table_name);

    // Buka file tabel
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printf("Error opening table file\n");
        return;
    }

    // Buka file sementara untuk menulis perubahan
    char temp_path[SMOL_BUF];
    snprintf(temp_path, sizeof(temp_path), "%s/%s/temp.txt", db_path, db_name);
    FILE *temp_file = fopen(temp_path, "w");
    if (temp_file == NULL) {
        printf("Error opening temporary file\n");
        fclose(file);
        return;
    }

    char line[BUFF_SIZE];
    while (fgets(line, sizeof(line), file)) {
        // Split baris menjadi kolom
        char *token = strtok(line, " ");
        if (token == NULL) {
            fprintf(temp_file, "%s", line);
            continue;
        }

        // Ambil nilai kolom pertama (sesuai dengan nama kolom yang ingin diupdate)
        if (strcmp(token, column_name) == 0) {
            // Ganti nilai kolom dengan nilai yang baru
            fprintf(temp_file, "%s %s\n", column_name, new_value);
        } else {
            fprintf(temp_file, "%s", line);
        }
    }

    // Tutup file
    fclose(file);
    fclose(temp_file);

    // Hapus file tabel lama
    remove(path);

    // Ubah nama file sementara menjadi nama file asli
    rename(temp_path, path);
}


//daemon ya daemon
void run_as_daemon() {
    pid_t pid;

    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Membuat socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed \n");
        exit(EXIT_FAILURE);
    }
    // Menetapkan informasi alamat server
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    // Binding socket ke alamat dan port tertentu
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }
    // Mendengarkan koneksi yang masuk
    if (listen(server_fd, MAX_PENDING_CONNECTIONS) < 0) {
        perror("Listen failed \n");
        exit(EXIT_FAILURE);
    }
    printf("Server running as daemon on port %d...\n", PORT);
    // Menjalankan sebagai daemon
    //run_as_daemon();
    // Menerima koneksi dan menangani permintaan
    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("Accept failed \n");
            exit(EXIT_FAILURE);
        }

        char buffer[1024] = {0};
        char cpy_buff[1024] = {0};
        int valread = recv(new_socket, buffer, 1024, 0);
        if (valread < 0) {
            perror("Error in receiving message from client \n");
            exit(EXIT_FAILURE);
        } else if (valread == 0) {
            printf("Client disconnected\n");
            close(new_socket);
            continue;
        }
        
        printf("Command received: %s\n", buffer);

        strcpy(cpy_buff, buffer); //salin buffer
        char *cmd_token = strtok(buffer, " "); //potong buffer

        if(strcmp(cmd_token, "LOGIN") == 0) db_login(new_socket, cmd_token);
        else if(strcmp(cmd_token, "CREATE") == 0) { 
            cmd_token = strtok(NULL, " ");

            if(strcmp(cmd_token, "USER") == 0) {
                if(arg_count(cpy_buff, 6)) mkusr(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "DATABASE") == 0) {
                if(arg_count(cpy_buff, 3)) CreateDatabase(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "TABLE") == 0) {
                if(!arg_count(cpy_buff, 3)) CreateTable(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "GRANT") == 0) {
            if(arg_count(cpy_buff, 5)) grnt_db(new_socket, cmd_token);
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "USE") == 0) {
            if(arg_count(cpy_buff, 2)) set_db(new_socket, cmd_token);
            else invalid_cmd(new_socket);
        }
        else if(strcmp(cmd_token, "DROP") == 0) {
            cmd_token = strtok(NULL, " ");

            if(strcmp(cmd_token, "DATABASE") == 0) {
                if(arg_count(cpy_buff, 3)) DropDatabase(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else if(strcmp(cmd_token, "TABLE") == 0) {
                if(arg_count(cpy_buff, 3)) DropTable(new_socket, cmd_token);
                else invalid_cmd(new_socket);
            }
            else invalid_cmd(new_socket);
        }
        else invalid_cmd(new_socket);
        close(new_socket); // Tutup socket untuk koneksi yang selesai
    }

    return 0;
}
